from django.db import models


class FileTransactionMetadata(models.Model):
    action_choices = ['c', 'r', 'u', 'd']
    id = models.AutoField(primary_key=True)
    file_id = models.IntegerField()
    action = models.CharField(max_length=1)
    timestamp = models.DateTimeField(auto_now_add=True)
    author = models.CharField(max_length=10)
    type = models.CharField(max_length=10)


class MultifileArchive(models.Model):
    id = models.AutoField(primary_key=True)
    file = models.FileField(upload_to='upload/multifile_archive/')
