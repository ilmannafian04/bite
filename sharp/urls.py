from django.urls import path

from sharp import views

urlpatterns = [
    path('metadata', views.new_metadata),
    path('multifile-compress', views.multifile_compression)
]
