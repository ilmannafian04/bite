import io
import json
import time
import types
import zipfile
from functools import partial
from json import JSONDecodeError

import after_response
import pika
import requests
from django.core.files.uploadedfile import SimpleUploadedFile
from django.http import HttpResponseNotFound, HttpResponseForbidden, HttpResponseBadRequest, JsonResponse, FileResponse
from django.shortcuts import redirect

from bite import settings
from sharp.models import FileTransactionMetadata, MultifileArchive


def index(request):
    return redirect('http://infralabs.cs.ui.ac.id:20061')


def new_metadata(request):
    if request.method == 'POST':
        if 'Authorization' in request.headers:
            if is_authorized(request.headers['Authorization']):
                try:
                    data = json.loads(request.body)
                except JSONDecodeError:
                    return HttpResponseBadRequest()
                if metadata_data_is_valid(data):
                    metadata = FileTransactionMetadata(
                        file_id=data['fileId'],
                        action=data['action'],
                        author=data['author'],
                        type=data['type']
                    )
                    metadata.save()
                    return JsonResponse({'success': True})
                else:
                    return HttpResponseBadRequest()
            else:
                return HttpResponseForbidden()
        else:
            return HttpResponseForbidden()
    elif request.method == 'GET':
        if 'Authorization' in request.headers:
            if is_authorized(request.headers['Authorization']):
                query = FileTransactionMetadata.objects.all()
                if 'offset' in request.GET:
                    try:
                        offset = int(request.GET['offset'])
                    except ValueError:
                        return HttpResponseBadRequest()
                    query = query[offset:]
                if 'limit' in request.GET:
                    try:
                        limit = int(request.GET['limit'])
                    except ValueError:
                        return HttpResponseBadRequest()
                    query = query[:limit]
                data = []
                for metadata in query:
                    data.append({
                        'fileId': metadata.file_id,
                        'action': metadata.action,
                        'timestamp': metadata.timestamp,
                        'author': metadata.author,
                        'type': metadata.type
                    })
                return JsonResponse({'metadatas': data})
            else:
                return HttpResponseForbidden()
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseNotFound()


def multifile_compression(request):
    if request.method == 'POST':
        if 'X-ROUTING-KEY' in request.headers:
            files = {}
            for file in request.FILES:
                files[request.FILES[file].name] = io.BytesIO(request.FILES[file].file.read())
            multifile_compression_task.after_response(files, request.headers['X-ROUTING-KEY'])
            return JsonResponse({'success': True})
        else:
            return HttpResponseBadRequest()
    else:
        return HttpResponseNotFound()


@after_response.enable
def multifile_compression_task(files, routing_key):
    time.sleep(3)
    size = 0
    for fileName in files:
        file = files[fileName]
        size += len(file.getvalue())

    credentials = pika.PlainCredentials(settings.RABBITMQ_USER, settings.RABBITMQ_PASSWORD)
    parameters = pika.ConnectionParameters(
        settings.RABBITMQ_HOST,
        settings.RABBITMQ_PORT,
        settings.RABBITMQ_VHOST,
        credentials
    )
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange=settings.NPM, exchange_type='direct')
    queue = channel.queue_declare(queue=routing_key)
    channel.queue_bind(exchange=settings.NPM, queue=queue.method.queue)

    # noinspection PyUnusedLocal
    def compress_callback(total_size, original_write, self, buf):
        compress_callback.compressed_size += len(buf)
        progress_percentage = compress_callback.compressed_size / size * 100 \
            if compress_callback.compressed_size / size * 100 <= 100 \
            else 100.0
        if progress_percentage - compress_callback.delta >= 10:
            channel.basic_publish(
                exchange=settings.NPM,
                routing_key=routing_key,
                body=f'compressing-{progress_percentage}-{size}-{len(files)}'
            )
            compress_callback.delta = int(progress_percentage) // 10
            compress_callback.delta *= 10
        return original_write(buf)

    compress_callback.compressed_size = 0
    compress_callback.delta = 0
    in_memory = io.BytesIO()
    in_memeory_zip = zipfile.ZipFile(in_memory, 'w', zipfile.ZIP_DEFLATED)
    in_memeory_zip.fp.write = types.MethodType(
        partial(compress_callback, size, in_memeory_zip.fp.write),
        in_memeory_zip.fp
    )

    file_counter = 0
    for fileName in files:
        file = files[fileName]
        in_memeory_zip.writestr(fileName, file.read())
        percentage = compress_callback.compressed_size / size * 100 \
            if compress_callback.compressed_size / size * 100 <= 100 \
            else 100.0
        file_counter += 1
        channel.basic_publish(
            exchange=settings.NPM,
            routing_key=routing_key,
            body=f'compressing-{percentage}-{size}-{len(files)}-{file_counter / len(files) * 100}'
        )
    for file in in_memeory_zip.filelist:
        file.create_system = 0
    in_memeory_zip.close()
    in_memory.seek(0)
    query = MultifileArchive(file=SimpleUploadedFile('archive.zip', in_memory.read()))
    query.save()
    in_memory.close()
    for fileName in files:
        files[fileName].close()
    channel.basic_publish(exchange=settings.NPM, routing_key=routing_key, body=f'compressing-100.0-{size}-{len(files)}')
    channel.basic_publish(exchange=settings.NPM, routing_key=routing_key, body=f'done-{query.id}')
    connection.close()


def download_arachive(request):
    if request.method == 'GET':
        if 'id' in request.GET:
            query_id = request.GET['id']
            try:
                query = MultifileArchive.objects.get(id=query_id)
            except MultifileArchive.DoesNotExist:
                return HttpResponseNotFound()
            return FileResponse(query.file, as_attachment=True)
        else:
            return HttpResponseBadRequest()
    else:
        return HttpResponseNotFound()


def is_authorized(token):
    headers = {'Authorization': token}
    resource_request = requests.get('http://oauth.infralabs.cs.ui.ac.id/oauth/resource', headers=headers)
    response = resource_request.json()
    if 'error' not in response:
        return True
    else:
        return False


def metadata_data_is_valid(data):
    if type(data['fileId']) is not int:
        return False
    if type(data['author']) is str:
        if data['action'] not in FileTransactionMetadata.action_choices:
            return False
    if type(data['author']) is str:
        if len(data['author']) != 10:
            return False
    else:
        return False
    if type(data['type']) is str:
        if data['type'] not in ['original', 'compressed']:
            return False
    else:
        return False
    return True
